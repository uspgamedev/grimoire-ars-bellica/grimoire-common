use std::collections::HashMap;
use std::fs::File;
use std::io::{BufRead, BufReader, Result};

use super::appdata;
use super::appearance::Appearance;
use super::tilemap::TileID;

pub struct GlyphPalette {
    _tile_to_glyph: HashMap<TileID, char>,
    _glyph_to_tile: HashMap<char, TileID>,
    _appearance_to_glyph: HashMap<Appearance, char>,
    _glyph_to_appearance: HashMap<char, Appearance>,
}

lazy_static! {
    pub static ref PALETTE: GlyphPalette = GlyphPalette::default();
}

fn read_tiles_palette(palette: &mut GlyphPalette) -> Result<()> {
    let tile_palette_data = File::open(appdata::appdata_dir().join("tiles.palette"))?;
    let mut reader = BufReader::new(tile_palette_data);
    let mut buffer = String::new();
    reader.read_line(&mut buffer)?;
    for (i, glyph) in buffer.trim_end().chars().enumerate() {
        let tile = TileID::from(i);
        palette._tile_to_glyph.insert(tile.clone(), glyph);
        palette._glyph_to_tile.insert(glyph, tile);
    }
    Ok(())
}

fn read_appearances_palette(palette: &mut GlyphPalette) -> Result<()> {
    let appearances_palette_data = File::open(appdata::appdata_dir().join("appearances.palette"))?;
    let mut reader = BufReader::new(appearances_palette_data);
    let mut buffer = String::new();
    reader.read_line(&mut buffer)?;
    for (i, glyph) in buffer.trim_end().chars().enumerate() {
        let appearance = Appearance::from(i);
        palette
            ._appearance_to_glyph
            .insert(appearance.clone(), glyph);
        palette._glyph_to_appearance.insert(glyph, appearance);
    }
    Ok(())
}

impl Default for GlyphPalette {
    fn default() -> Self {
        let mut palette = GlyphPalette {
            _tile_to_glyph: HashMap::new(),
            _glyph_to_tile: HashMap::new(),
            _appearance_to_glyph: HashMap::new(),
            _glyph_to_appearance: HashMap::new(),
        };
        read_tiles_palette(&mut palette).unwrap();
        read_appearances_palette(&mut palette).unwrap();
        palette
    }
}

impl GlyphPalette {
    pub fn tile_to_glyph(&self, id: &TileID) -> char {
        self._tile_to_glyph[id]
    }

    pub fn glyph_to_tile(&self, glyph: char) -> TileID {
        self._glyph_to_tile[&glyph].clone()
    }

    pub fn appearance_to_glyph(&self, appearance: &Appearance) -> char {
        self._appearance_to_glyph[appearance]
    }

    pub fn glyph_to_appearance(&self, glyph: char) -> Appearance {
        self._glyph_to_appearance[&glyph].clone()
    }
}
