use super::datatable::DataTable;

#[derive(Serialize, Deserialize)]
pub enum SessionRequest {
    TryAction(String, DataTable),
    EndSession,
}
