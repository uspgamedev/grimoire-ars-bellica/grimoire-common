use std::cmp::{max, min};
use std::convert::From;
use std::fmt;
use std::ops::{Add, Index, IndexMut, Mul, Sub};
use toml::Value;

pub trait IntN {
    fn size(&self) -> usize;
    fn get_i(&self) -> usize;
    fn get_j(&self) -> usize;
    fn get_k(&self) -> usize {
        0
    }
    fn get_x_component(&self) -> Self;
    fn get_y_component(&self) -> Self;
    fn distance_to(&self, other: &Self) -> f64;
    fn is_within_distance_of(&self, other: &Self, distance: f64) -> bool;
}

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize, Default, Debug)]
pub struct Int2 {
    pub x: i64,
    pub y: i64,
}

#[derive(PartialEq, Clone, Copy, Serialize, Deserialize, Default, Debug)]
pub struct Int3 {
    pub x: i64,
    pub y: i64,
    pub z: i64,
}

impl Int3 {
    pub fn dot(self, other: &Int3) -> i64 {
        self.x * other.x + self.y * other.y + self.z * other.z
    }

    pub const fn new(x: i64, y: i64, z: i64) -> Self {
        Int3 { x, y, z }
    }

    pub fn unit(&self) -> Self {
        Int3 {
            x: max(min(self.x, 1), -1),
            y: max(min(self.y, 1), -1),
            z: max(min(self.z, 1), -1),
        }
    }

    pub fn get_z_component(&self) -> Self {
        Int3 {
            x: 0,
            y: 0,
            z: self.z,
        }
    }

    pub fn get_k_component(&self) -> Self {
        self.get_z_component()
    }

    pub fn unit_z() -> Int3 {
        Int3 { x: 0, y: 0, z: 1 }
    }

    pub fn unit_k() -> Int3 {
        Self::unit_z()
    }
}

impl Int2 {
    pub fn dot(&self, other: &Int2) -> i64 {
        self.x * other.x + self.y * other.y
    }

    pub const fn new(x: i64, y: i64) -> Self {
        Self { x, y }
    }

    pub fn unit(&self) -> Self {
        Int2 {
            x: max(min(self.x, 1), -1),
            y: max(min(self.y, 1), -1),
        }
    }
}

impl IntN for Int2 {
    fn size(&self) -> usize {
        self.x.abs() as usize + self.y.abs() as usize
    }

    fn get_i(&self) -> usize {
        self.y as usize
    }

    fn get_j(&self) -> usize {
        self.x as usize
    }

    fn get_x_component(&self) -> Self {
        Int2 { x: self.x, y: 0 }
    }

    fn get_y_component(&self) -> Self {
        Int2 { x: 0, y: self.y }
    }

    fn distance_to(&self, other: &Self) -> f64 {
        let diff = self - other;
        (diff.dot(&diff) as f64).sqrt()
    }

    fn is_within_distance_of(&self, other: &Self, distance: f64) -> bool {
        let diff = self - other;
        (diff.dot(&diff) as f64) <= distance * distance
    }
}

impl IntN for Int3 {
    fn size(&self) -> usize {
        self.x.abs() as usize + self.y.abs() as usize + self.z.abs() as usize
    }

    fn get_i(&self) -> usize {
        self.y as usize
    }

    fn get_j(&self) -> usize {
        self.x as usize
    }

    fn get_k(&self) -> usize {
        self.z as usize
    }

    fn get_x_component(&self) -> Self {
        Int3 {
            x: self.x,
            y: 0,
            z: 0,
        }
    }

    fn get_y_component(&self) -> Self {
        Int3 {
            x: 0,
            y: self.y,
            z: 0,
        }
    }

    fn distance_to(&self, other: &Self) -> f64 {
        let diff = self - other;
        (diff.dot(&diff) as f64).sqrt()
    }

    fn is_within_distance_of(&self, other: &Self, distance: f64) -> bool {
        let diff = self - other;
        (diff.dot(&diff) as f64) <= distance * distance
    }
}

impl<'a> Mul<&'a i64> for Int2 {
    type Output = Int2;

    fn mul(self, scalar: &'a i64) -> Int2 {
        Int2 {
            x: self.x * scalar,
            y: self.y * scalar,
        }
    }
}

impl<'a> Mul<&'a i64> for Int3 {
    type Output = Int3;

    fn mul(self, scalar: &'a i64) -> Int3 {
        Int3 {
            x: self.x * scalar,
            y: self.y * scalar,
            z: self.z * scalar,
        }
    }
}

impl<'a> Mul<&'a Int2> for Int2 {
    type Output = Int2;

    fn mul(self, other: &'a Int2) -> Int2 {
        Int2 {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}

impl<'a> Mul<&'a Int3> for Int3 {
    type Output = Int3;

    fn mul(self, other: &'a Int3) -> Int3 {
        Int3 {
            x: self.x * other.x,
            y: self.y * other.y,
            z: self.z * other.z,
        }
    }
}

impl Sub for Int2 {
    type Output = Int2;

    fn sub(self, other: Int2) -> Int2 {
        Int2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'b> Sub<&'b Int2> for Int2 {
    type Output = Int2;

    fn sub(self, other: &'b Int2) -> Int2 {
        Int2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a, 'b> Sub<&'b Int2> for &'a Int2 {
    type Output = Int2;

    fn sub(self, other: &'b Int2) -> Int2 {
        Int2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl<'a> Sub<Int2> for &'a Int2 {
    type Output = Int2;

    fn sub(self, other: Int2) -> Int2 {
        Int2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Add for Int2 {
    type Output = Int2;

    fn add(self, other: Int2) -> Int2 {
        Int2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'b> Add<&'b Int2> for Int2 {
    type Output = Int2;

    fn add(self, other: &'b Int2) -> Int2 {
        Int2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'a, 'b> Add<&'b Int2> for &'a Int2 {
    type Output = Int2;

    fn add(self, other: &'b Int2) -> Int2 {
        Int2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl<'a> Add<Int2> for &'a Int2 {
    type Output = Int2;

    fn add(self, other: Int2) -> Int2 {
        Int2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Int3 {
    type Output = Int3;

    fn sub(self, other: Int3) -> Int3 {
        Int3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<'b> Sub<&'b Int3> for Int3 {
    type Output = Int3;

    fn sub(self, other: &'b Int3) -> Int3 {
        Int3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<'a, 'b> Sub<&'b Int3> for &'a Int3 {
    type Output = Int3;

    fn sub(self, other: &'b Int3) -> Int3 {
        Int3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl<'a> Sub<Int3> for &'a Int3 {
    type Output = Int3;

    fn sub(self, other: Int3) -> Int3 {
        Int3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Add for Int3 {
    type Output = Int3;

    fn add(self, other: Int3) -> Int3 {
        Int3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<'b> Add<&'b Int3> for Int3 {
    type Output = Int3;

    fn add(self, other: &'b Int3) -> Int3 {
        Int3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<'a, 'b> Add<&'b Int3> for &'a Int3 {
    type Output = Int3;

    fn add(self, other: &'b Int3) -> Int3 {
        Int3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl<'a> Add<Int3> for &'a Int3 {
    type Output = Int3;

    fn add(self, other: Int3) -> Int3 {
        Int3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Index<usize> for Int2 {
    type Output = i64;

    fn index(&self, idx: usize) -> &i64 {
        match idx {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("Index out of Bounds for Int2")
        }
    }
}

impl Index<usize> for Int3 {
    type Output = i64;

    fn index(&self, idx: usize) -> &i64 {
        match idx {
            0 => &self.x,
            1 => &self.y,
            2 => &self.z,
            _ => panic!("Index out of Bounds for Int3")
        }
    }
}

impl IndexMut<usize> for Int2 {
    fn index_mut(&mut self, idx: usize) -> &mut i64 {
        match idx {
            0 => &mut self.x,
            1 => &mut self.y,
            _ => panic!("Index out of Bounds for Int2")
        }
    }
}

impl IndexMut<usize> for Int3 {
    fn index_mut(&mut self, idx: usize) -> &mut i64 {
        match idx {
            0 => &mut self.x,
            1 => &mut self.y,
            2 => &mut self.z,
            _ => panic!("Index out of Bounds for Int3")
        }
    }
}

impl From<[i64; 2]> for Int2 {
    fn from(item: [i64; 2]) -> Self {
        Int2 {
            x: item[0],
            y: item[1],
        }
    }
}

impl From<Vec<i64>> for Int3 {
    fn from(item: Vec<i64>) -> Self {
        Int3 {
            x: item[0],
            y: item[1],
            z: item[2],
        }
    }
}

impl From<[i64; 3]> for Int3 {
    fn from(item: [i64; 3]) -> Self {
        Int3 {
            x: item[0],
            y: item[1],
            z: item[2],
        }
    }
}

impl From<Vec<Value>> for Int3 {
    fn from(item: Vec<Value>) -> Self {
        if let (Value::Integer(x), Value::Integer(y), Value::Integer(z)) = (item[0].clone(), item[1].clone(), item[2].clone()) {
            Int3 { x, y, z }
        } else {
            panic!["Could not match value into integer"]
        }
    }
}

impl From<Int3> for Value {
    fn from(item: Int3) -> Value {
        Value::Array(vec![Value::from(item.x), Value::from(item.y), Value::from(item.z)])
    }
}

impl<'a> From<&'a Int3> for Int2 {
    fn from(item: &'a Int3) -> Self {
        Int2 {
            x: item.x,
            y: item.y,
        }
    }
}

impl From<Int3> for Int2 {
    fn from(item: Int3) -> Self {
        Int2::from(&item)
    }
}

impl<'a> From<&'a Int2> for Int3 {
    fn from(item: &'a Int2) -> Self {
        Int3 {
            x: item.x,
            y: item.y,
            z: 0,
        }
    }
}

impl From<Int2> for Int3 {
    fn from(item: Int2) -> Self {
        Int3::from(&item)
    }
}

impl fmt::Display for Int2 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.x, self.y)
    }
}

impl fmt::Display for Int3 {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {}, {})", self.x, self.y, self.z)
    }
}
