use super::session_state::SessionState;

#[derive(Serialize, Deserialize)]
pub enum Motive {
    UserTurn,
    UnknownAction,
    MissingInput(String),
    InvalidInput(String),
    ImpossibleAction,
}

#[derive(Serialize, Deserialize)]
pub enum SessionResponse {
    ActionPending(Motive),
    StateChanged(SessionState),
    NoPlayer,
}
