use super::intn::{Int2, Int3};

#[derive(PartialEq, Clone, Debug, Serialize, Deserialize)]
pub enum Direction {
    NONE = 0,
    UP = 1,
    DOWN = 2,
    LEFT = 3,
    RIGHT = 4,
    UPLEFT = 5,
    UPRIGHT = 6,
    DOWNLEFT = 7,
    DOWNRIGHT = 8,
}

pub const DIRECTIONS: [Int2; 9] = [
    Int2::new(0, 0),
    Int2::new(0, -1),
    Int2::new(0, 1),
    Int2::new(-1, 0),
    Int2::new(1, 0),
    Int2::new(-1, -1),
    Int2::new(1, -1),
    Int2::new(-1, 1),
    Int2::new(1, 1),
];

impl From<usize> for Direction {
    fn from(number: usize) -> Self {
        match number {
            1 => Direction::UP,
            2 => Direction::DOWN,
            3 => Direction::LEFT,
            4 => Direction::RIGHT,
            5 => Direction::UPLEFT,
            6 => Direction::UPRIGHT,
            7 => Direction::DOWNLEFT,
            8 => Direction::DOWNRIGHT,
            _ => Direction::NONE,
        }
    }
}

impl From<i64> for Direction {
    fn from(number: i64) -> Self {
        Self::from(number as usize)
    }
}

impl From<Int2> for Direction {
    fn from(int2: Int2) -> Self {
        for (idx, direction) in DIRECTIONS.iter().enumerate() {
            if int2 == *direction {
                return Direction::from(idx);
            }
        }
        Direction::NONE
    }
}

impl From<Int3> for Direction {
    fn from(int3: Int3) -> Self {
        Direction::from(Int2::from(int3))
    }
}

impl From<Direction> for Int3 {
    fn from(dir: Direction) -> Int3 {
        Int3::from(Int2::from(dir))
    }
}

impl From<Direction> for Int2 {
    fn from(dir: Direction) -> Int2 {
        DIRECTIONS[dir as usize].clone()
    }
}
