#[derive(Serialize, Deserialize, Clone, PartialEq)]
pub enum Command {
    INVALID,
    UP,
    DOWN,
    LEFT,
    RIGHT,
    UPLEFT,
    UPRIGHT,
    DOWNLEFT,
    DOWNRIGHT,
    CONFIRM,  // F
    CANCEL,   // D
    FIRE,     // S
    EXTRA,    // A
    INFO,     // R
    INTERACT, // E
    MENU,     // W
    QUIT,     // Q
}
