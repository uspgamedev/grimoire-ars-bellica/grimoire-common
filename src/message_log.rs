#[derive(Clone, Serialize, Deserialize, Default)]
pub struct MessageLog {
    message_entries: Vec<String>,
}

impl MessageLog {
    pub fn message(&mut self, entry: String) {
        self.message_entries.push(entry);
    }

    pub fn message_entry(&self, idx: i64) -> String {
        let size = self.message_entries.len() as i64;
        if idx >= size || -idx > size {
            return String::from("");
        }
        self.message_entries[((idx + size) % size) as usize].clone()
    }
}
