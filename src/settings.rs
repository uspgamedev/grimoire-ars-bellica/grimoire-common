extern crate serde;
extern crate toml;

use super::appdata;

use std::collections::HashMap;
use std::fs;
use std::io::Write;
use std::io::{Error, ErrorKind, Result};
use std::path::PathBuf;

use super::file::read_file;
use super::command::Command;

#[derive(Serialize, Deserialize)]
pub struct TOMLKeybindings {
    default: HashMap<String, char>,
}

#[derive(Serialize, Deserialize)]
pub struct Keybindings {
    default: HashMap<char, Command>,
}

#[derive(Serialize, Deserialize)]
pub struct TOMLSettings {
    version: String,
    keybindings: HashMap<String, char>,
}

#[derive(Serialize, Deserialize)]
pub struct Settings {
    version: String,
    keybindings: Keybindings,
}

impl Default for Settings {
    fn default() -> Self {
        let path = appdata::settings_file_path();
        match read_file(&path) {
            Err(e) => panic!("Error reading settings file at {:?} {:?}", path, e),
            Ok(file_string) => match toml::from_str::<TOMLSettings>(file_string.as_str()) {
                Err(e) => panic!("Error reading toml settings from file. {:?}", e),
                Ok(toml_settings) => Settings::from_toml_settings(toml_settings),
            },
        }
    }
}

impl Settings {
    pub fn from_toml_settings(toml_settings: TOMLSettings) -> Self {
        Settings {
            version: toml_settings.version,
            keybindings: Keybindings::from_toml_keybindings(&toml_settings.keybindings),
        }
    }

    pub fn get_version(&self) -> &String {
        &self.version
    }

    pub fn get_keybindings(&self) -> &Keybindings {
        &self.keybindings
    }
}

impl Keybindings {
    pub fn get(&self, key: char) -> Option<&Command> {
        self.default.get(&key)
    }

    pub fn from_toml_keybindings(toml_keybindings: &HashMap<String, char>) -> Self {
        let mut default_bindings: HashMap<char, Command> = HashMap::new();
        let name_to_command: HashMap<&str, Command> = [
            ("up", Command::UP),
            ("down", Command::DOWN),
            ("left", Command::LEFT),
            ("right", Command::RIGHT),
            ("upleft", Command::UPLEFT),
            ("upright", Command::UPRIGHT),
            ("downleft", Command::DOWNLEFT),
            ("downright", Command::DOWNRIGHT),
            ("confirm", Command::CONFIRM),
            ("cancel", Command::CANCEL),
            ("fire", Command::FIRE),
            ("extra", Command::EXTRA),
            ("info", Command::INFO),
            ("interact", Command::INTERACT),
            ("menu", Command::MENU),
            ("quit", Command::QUIT),
        ]
        .iter()
        .cloned()
        .collect();
        for (command_name, key) in toml_keybindings {
            default_bindings.insert(key.clone(), name_to_command[command_name.as_str()].clone());
        }
        Keybindings {
            default: default_bindings,
        }
    }
}

fn read_toml_keybindings(path: PathBuf) -> HashMap<String, char> {
    match read_file(&path) {
        Err(e) => panic!("Error reading toml file at {:?} {:?}", path, e),
        Ok(file_string) => match toml::from_str::<TOMLKeybindings>(file_string.as_str()) {
            Err(e) => panic!("Error reading toml string from file. {:?}", e),
            Ok(keybindings) => keybindings.default,
        },
    }
}

pub fn bootstrap(file: &mut fs::File) -> Result<()> {
    let keybindings_path = appdata::appdata_dir().join("keybindings.toml");
    let toml_settings = TOMLSettings {
        version: String::from(env!("CARGO_PKG_VERSION")),
        keybindings: read_toml_keybindings(keybindings_path),
    };
    match toml::to_string(&toml_settings) {
        Err(e) => Err(Error::new(ErrorKind::InvalidInput, e)),
        Ok(toml_string) => file.write_all(toml_string.as_bytes())
    }
}
