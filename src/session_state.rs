use super::appearance::Appearance;
use super::intn::Int3;
use super::tilemap::TileMap;
use super::identity::ID;

#[derive(Serialize, Deserialize)]
pub struct SessionState {
    pub visible_tilemap: TileMap,
    pub entities_info: Vec<EntityInfo>,
    pub player_state: PlayerState,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct EntityInfo {
    pub id: ID,
    pub maybe_position: Option<Int3>,
    pub passable: bool,
    pub appearance: Appearance,
    pub name: String,
    pub possible_actions: Vec<String>,
}

#[derive(Serialize, Deserialize)]
pub struct PlayerState {
    pub info_idx: usize,
    pub hp: usize,
    pub max_hp: usize,
    pub reach: usize,
    pub sight_range: usize,
    pub move_action: String,
    pub attack_action: String,
    pub inventory: Vec<usize>,
    pub journal: Vec<String>,
}

impl SessionState {
    pub fn player_info(&self) -> &EntityInfo {
        &self.entities_info[self.player_state.info_idx]
    }

    pub fn get_entity_infos_at(&self, pos: &Int3) -> Vec<EntityInfo> {
        self.entities_info
            .iter()
            .filter(|x| match x.maybe_position.as_ref() {
                Some(position) => position == pos,
                None => false,
            })
            .map(|x| x.clone())
            .collect()
    }

    pub fn get_entity_info_at(&self, pos: &Int3) -> Option<EntityInfo> {
        match self.get_entity_infos_at(pos).first() {
            Some(entity_info) => Some(entity_info.clone()),
            None => None,
        }
    }
}

pub fn find_entity_info(session_state: &SessionState, id: ID) -> Option<EntityInfo> {
    for entity_info in session_state.entities_info.iter() {
        if entity_info.id == id {
            return Some(entity_info.clone());
        }
    }
    None
}
