use super::aliases::*;

pub const PICKUP: StringLiteral = "pick_up";
pub const OPEN: StringLiteral = "open";
pub const CLOSE: StringLiteral = "close";
pub const STRIKE: StringLiteral = "strike_with";
pub const MOVE: StringLiteral = "move";
pub const IDLE: StringLiteral = "idle";
