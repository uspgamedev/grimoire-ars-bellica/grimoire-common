extern crate toml;

#[macro_export]
macro_rules! get_value{
    ($table:expr, $key:expr, i64) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, i64),
            None => 0,
        }
    };
    ($table:expr, $key:expr, Option<ID>) => {
        match $table.get(&String::from($key)) {
            Some(x) => Some(match_val_type!(x, i64) as ID),
            None => None,
        }
    };
    ($table:expr, $key:expr, f64) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, f64),
            None => 0.0,
        }
    };
    ($table:expr, $key:expr, bool) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, bool),
            None => false,
        }
    };
    ($table:expr, $key:expr, str) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, str),
            None => "",
        }
    };
    ($table:expr, $key:expr, Vec<$type:ty>) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, Vec<$type>),
            None => Vec::new(),
        }
    };
    ($table:expr, $key:expr, HashMap<String, $type:ty>) => {
        match $table.get(&String::from($key)) {
            Some(x) => match_val_type!(x, HashMap<String, $type>),
            None => HashMap::new(),
        }
    };
}

#[macro_export]
macro_rules! match_val_type {
    ($val:expr, i64) => {
        $val.as_integer().unwrap()
    };
    ($val:expr, f64) => {
        $val.as_float().unwrap()
    };
    ($val:expr, bool) => {
        $val.as_bool().unwrap()
    };
    ($val:expr, str) => {
        $val.as_str().unwrap()
    };
    ($val:expr, Vec<$type2:ty>) => {
        $val.as_array()
            .unwrap()
            .iter()
            .map(|x| x.as_integer().unwrap() as $type2)
            .collect()
    };
    ($val:expr, HashMap<String, $type2:ty>) => {
        $val.as_table().unwrap()
    };
}

pub type DataTable = toml::map::Map<String, toml::value::Value>;

#[macro_export]
macro_rules! datatable {
    ( $( $key:expr => $value:expr ),* ) => {
        {
            let mut temp_vec = DataTable::new();
            $(
                temp_vec.insert(String::from($key), toml::Value::from($value));
            )*
            temp_vec
        }
    };
}
