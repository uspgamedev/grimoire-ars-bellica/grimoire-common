use std::fs;
use std::io::{BufReader, Read};
use std::path::PathBuf;

pub fn read_file(path: &PathBuf) -> std::io::Result<String> {
    let file = fs::File::open(path)?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;
    Ok(contents)
}
