use super::intn::{Int3, IntN};

#[derive(Debug, PartialEq, Clone, Hash, Eq, Serialize, Deserialize)]
pub enum TileID {
    EMPTY = 0,
    ROCK = 1,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TileMap {
    w: usize,
    h: usize,
    d: usize,
    tiles: Vec<TileID>,
}

impl From<usize> for TileID {
    fn from(i: usize) -> Self {
        match i {
            1 => TileID::ROCK,
            _ => TileID::EMPTY,
        }
    }
}

fn number_repr(c: char) -> u8 {
    (c as i64 - '0' as i64) as u8
}

fn read_dec(a: char, b: char) -> TileID {
    TileID::from((number_repr(a) * 10 + number_repr(b)) as usize)
}

impl TileMap {
    pub fn new(tile_data: &str, w: usize, h: usize, d: usize) -> Self {
        let mut tilemap = TileMap {
            w,
            h,
            d,
            tiles: Vec::<TileID>::new(),
        };
        let len = w * h * d;
        let mut chars = tile_data.matches(|c: char| !c.is_whitespace());
        for _ in 0..len {
            let first = match chars.next() {
                Some(c) => c.chars().next().unwrap(),
                None => panic!("Tile map data too small for dimensions")
            };
            let second = match chars.next() {
                Some(c) => c.chars().next().unwrap(),
                None => panic!("Tile map data too small for dimensions")
            };
            tilemap.tiles.push(read_dec(first, second));
        }
        tilemap
    }

    pub fn is_tile_inside(&self, pos: &Int3) -> bool {
        pos.get_i() < self.h && pos.get_j() < self.w && pos.get_k() < self.d
    }

    pub fn get_tile(&self, pos: &Int3) -> TileID {
        self.tiles[self.get_index(pos.get_i(), pos.get_j(), pos.get_k())].clone()
    }

    pub fn get_index(&self, i: usize, j: usize, k: usize) -> usize {
        k * (self.w * self.h) + i * self.w + j
    }

    pub fn get_width(&self) -> usize {
        self.w
    }

    pub fn get_height(&self) -> usize {
        self.h
    }

    pub fn get_depth(&self) -> usize {
        self.d
    }

    pub fn get_tile_data(&self) -> String {
        let mut data = String::new();
        for k in 0..self.d {
            for i in 0..self.h {
                for j in 0..self.w {
                    data.push_str(
                        write_dec(self.get_tile(&Int3::new(j as i64, i as i64, k as i64))).as_str(),
                    )
                }
                data.push('\n');
            }
            data.push('\n');
            data.push('\n');
        }
        data
    }
}

fn write_dec(t: TileID) -> String {
    let mut string = String::new();
    string.push((((t.clone() as i64) / 10 + ('0' as i64)) as u8) as char);
    string.push((((t as i64) % 10 + ('0' as i64)) as u8) as char);
    string
}
