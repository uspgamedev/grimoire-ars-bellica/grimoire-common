extern crate dirs;

use std::fs;
use std::fs::DirBuilder;
use std::io::{Error, ErrorKind, Result};
use std::path::{Path, PathBuf};

use super::settings;

fn root_dir() -> PathBuf {
    dirs::data_local_dir().unwrap().join("grimoire-ars-bellica")
}

pub fn db_dir() -> PathBuf {
    ["./database"].iter().collect()
}

pub fn appdata_dir() -> PathBuf {
    root_dir().join("appdata")
}

pub fn save_dir() -> PathBuf {
    root_dir().join("saves")
}

pub fn settings_file_path() -> PathBuf {
    root_dir().join("settings.toml")
}

fn bootstrap_root() -> Result<()> {
    let root_path = root_dir();

    match fs::metadata(&root_path) {
        Err(_) => DirBuilder::new().create(root_path),
        Ok(root_path_metadata) => {
            if !root_path_metadata.is_dir() {
                Err(Error::new(
                    ErrorKind::AlreadyExists,
                    "Root path already exists but is not a directory!",
                ))
            } else {
                Ok(())
            }
        }
    }
}

fn bootstrap_appdata() -> Result<()> {
    let appdata_path = appdata_dir();
    let appdata_source_path = Path::new("./appdata");

    match fs::metadata(&appdata_path) {
        Err(_) => {
            let appdata_source_metadata = fs::metadata(&appdata_source_path)?;
            if appdata_source_metadata.is_dir() {
                let file_iterator = fs::read_dir(&appdata_source_path)?;
                DirBuilder::new().create(&appdata_path)?;
                for file in file_iterator {
                    let file_path = &file.unwrap().path();
                    let destination = appdata_path.join(file_path.file_name().unwrap());
                    fs::copy(file_path, &destination)?;
                }
                Ok(())
            } else {
                Err(Error::new(
                    ErrorKind::InvalidInput,
                    "Invalid appdata folder",
                ))
            }
        }
        Ok(_) => Ok(()),
    }
}

fn bootstrap_settings() -> Result<()> {
    let settings_path = settings_file_path();

    if let Err(_) = fs::metadata(&settings_path) {
        let mut settings_file = fs::File::create(&settings_path)?;
        settings::bootstrap(&mut settings_file)
    } else {
        Ok(())
    }
}

fn bootstrap_save_dir() -> Result<()> {
    let save_path_buf = save_dir();

    if fs::metadata(&save_path_buf).is_err() {
        DirBuilder::new().create(save_path_buf)?;
    }
    Ok(())
}

pub fn bootstrap() -> Result<()> {
    bootstrap_root()?;
    bootstrap_appdata()?;
    bootstrap_settings()?;
    bootstrap_save_dir()?;

    Ok(())
}
