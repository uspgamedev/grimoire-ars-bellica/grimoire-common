#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize)]
pub enum Appearance {
    INDESCRIBABLE,
    PLAYER,
    OpenDoor,
    ClosedDoor,
    ENEMY,
    TREASURE,
}

impl From<usize> for Appearance {
    fn from(i: usize) -> Self {
        match i {
            1 => Appearance::PLAYER,
            2 => Appearance::OpenDoor,
            3 => Appearance::ClosedDoor,
            4 => Appearance::ENEMY,
            5 => Appearance::TREASURE,
            _ => Appearance::INDESCRIBABLE,
        }
    }
}

impl From<i64> for Appearance {
    fn from(i: i64) -> Self {
        match i {
            1 => Appearance::PLAYER,
            2 => Appearance::OpenDoor,
            3 => Appearance::ClosedDoor,
            4 => Appearance::ENEMY,
            5 => Appearance::TREASURE,
            _ => Appearance::INDESCRIBABLE,
        }
    }
}
